<?php
declare(strict_types=1);

use Clue\Hexdump\Hexdump;
use SkyDiablo\DhcpServer\DHCPPacket\Context;
use SkyDiablo\DhcpServer\DHCPPacket\Options\Enum\MessageType;
use SkyDiablo\DhcpServer\DhcpServer;
use SkyDiablo\DhcpServer\DhcpServerEvent;
use SkyDiablo\DhcpServer\ServerEvent;
use SkyDiablo\DhcpServer\SocketServer;

require __DIR__ . '/../vendor/autoload.php';

$options = getopt('', [
    'host:',
    'port::',
    'interface:',
]);

$host = $options['host'] ?? '255.255.255.255';
$port = (int)($options['port'] ?? SocketServer::DEFAULT_PORT);

$socket = new SocketServer($host, $port, $options['interface']);
$dhcpd = new DhcpServer($socket);
$dhcpd->on(ServerEvent::ERROR->value, function (\Throwable $e) {
    echo 'ERROR: ' . $e->getMessage() . PHP_EOL . PHP_EOL;
})->on(DhcpServerEvent::PACKET_RECEIVED->value, function (Context $context) {

    $messageType = $context->getRequestPacket()->getMessageType();

    echo 'Received packet from ' . $context->getPeer() . PHP_EOL;
    echo 'Packet X-ID: 0x' . dechex($context->getRequestPacket()->getXid()) . PHP_EOL;
    echo 'Option-Count: ' . count($context->getRequestPacket()->getOptions()) . PHP_EOL;
    echo 'MessageType: ' . $messageType->name . PHP_EOL;

    switch ($messageType) { // checkout https://datatracker.ietf.org/doc/html/rfc2131#page-15
        case MessageType::DHCPDISCOVER:
            $context->getResponsePacket()->setMessageType(MessageType::DHCPOFFER);
            break;
        case MessageType::DHCPREQUEST:
            $context->getResponsePacket()->setMessageType(MessageType::DHCPACK); // or MessageType::DHCPDECLINE
            break;
        case MessageType::DHCPRELEASE:

            break;
    }

})->on(DhcpServerEvent::RAW_PACKET_RECEIVED->value, function ($message, $peer, $socket) {
//    echo 'RAW Received packet from ' . $peer . PHP_EOL;
//    $hexDumper = new Hexdump();
//    echo $hexDumper->dump($message);
});

echo sprintf('Starting DHCP server on %s:%d (Interface: %s)', $host, $port, $options['interface']), PHP_EOL;