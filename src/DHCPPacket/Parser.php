<?php
declare(strict_types=1);

namespace SkyDiablo\DhcpServer\DHCPPacket;

use Darsyn\IP\Version\IPv4;
use SkyDiablo\DhcpServer\DHCPPacket\Options\Parser as OptionParser;
use SkyDiablo\DhcpServer\DHCPPacket\Options\Serializer\Manager;
use SkyDiablo\DhcpServer\Helper\UnPackInteger;

enum DHCPPacketField: int
{
    case OP = 0;
    case HTYPE = 1;
    case HLEN = 2;
    case HOPS = 3;
    case XID = 4;
    case SECS = 8;
    case FLAGS = 10;
    case CIADDR = 12;
    case YIADDR = 16;
    case SIADDR = 20;
    case GIADDR = 24;
    case CHADDR = 28;
    case SNAME = 44;
    case FILE = 108;
    case MAGIC = 236;
    case OPTIONS = 240;
}

class Parser
{

    use UnPackInteger;

    protected OptionParser $optionParser;

    public function __construct(OptionParser $optionParser = null)
    {
        $this->optionParser = $optionParser ?? new OptionParser(new Manager());
    }

    /**
     * @param string $message
     * @return DHCPPacket
     * @throws \Darsyn\IP\Exception\InvalidIpAddressException
     * @throws \Darsyn\IP\Exception\WrongVersionException
     * @throws \PhpExtended\Parser\ParseThrowable
     * @throws \RuntimeException
     */
    public function parse(string $message): DHCPPacket
    {
        $packet = (new DHCPPacket())
            ->setOp(DHCPMessageOpCode::tryFrom($this->unpackInt8($message, DHCPPacketField::OP->value)) ?? throw new \RuntimeException('Invalid OP code'))
            ->setHtype(HardwareAddressType::tryFrom($this->unpackInt8($message, DHCPPacketField::HTYPE->value)) ?? throw new \RuntimeException('Invalid HTYPE'))
            ->setHlen($hlen = $this->unpackInt8($message, DHCPPacketField::HLEN->value))
            ->setHops($this->unpackInt8($message, DHCPPacketField::HOPS->value))
            ->setXid($this->unpackInt32($message, DHCPPacketField::XID->value))
            ->setSecs($this->unpackInt16($message, DHCPPacketField::SECS->value))
            ->setFlags([DHCPFlag::tryFrom($this->unpackInt16($message, DHCPPacketField::FLAGS->value)) ?? throw new \RuntimeException('Invalid FLAGS')])
            ->setCiaddr(IPv4::factory(substr($message, DHCPPacketField::CIADDR->value, 4)))
            ->setYiaddr(IPv4::factory(substr($message, DHCPPacketField::YIADDR->value, 4)))
            ->setSiaddr(IPv4::factory(substr($message, DHCPPacketField::SIADDR->value, 4)))
            ->setGiaddr(IPv4::factory(substr($message, DHCPPacketField::GIADDR->value, 4)))
            ->setChaddr(substr(unpack('H*', substr($message, DHCPPacketField::CHADDR->value, 16))[1], 0, $hlen * 2)) // TODO: use value-class for MAC instance
            ->setSname(substr($message, DHCPPacketField::SNAME->value, 64))
            ->setFile(substr($message, DHCPPacketField::FILE->value, 128));
        if ($this->hasMagicCookies($message)) {
            $options = $this->optionParser->parse(substr($message, DHCPPacketField::OPTIONS->value));
            $packet = $packet->setOptions($options);
        }

        return $packet;
    }

    protected function hasMagicCookies(string $message): bool
    {
        return substr($message, DHCPPacketField::MAGIC->value, 4) === DHCPPacket::MAGIC_COOKIE;
    }


}