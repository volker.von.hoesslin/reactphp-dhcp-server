<?php
declare(strict_types=1);

namespace SkyDiablo\DhcpServer\DHCPPacket\Options;

use Darsyn\IP\Version\IPv4;

class IPv4Option extends AbstractOption
{

    protected $ip;

    public function __construct(int $code, IPv4 $ip)
    {
        parent::__construct($code);
        $this->ip = $ip;
    }

    public function getValue(): IPv4
    {
        return $this->ip;
    }

    public function __clone(): void
    {
        $this->ip = clone $this->ip;
    }


}