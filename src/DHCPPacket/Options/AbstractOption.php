<?php
declare(strict_types=1);

namespace SkyDiablo\DhcpServer\DHCPPacket\Options;

abstract class AbstractOption implements OptionInterface
{

    protected int $code;

    /**
     * @param int $code
     */
    public function __construct(int $code)
    {
        $this->code = $code;
    }


    public function getCode(): int
    {
        return $this->code;
    }


}