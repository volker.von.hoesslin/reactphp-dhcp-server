<?php
declare(strict_types=1);

namespace SkyDiablo\DhcpServer\DHCPPacket\Options;

class BooleanOption extends AbstractOption
{

    protected bool $value;

    public function __construct(int $code, bool $value)
    {
        parent::__construct($code);
        $this->value = $value;
    }

    public function getValue(): bool
    {
        return $this->value;
    }

}