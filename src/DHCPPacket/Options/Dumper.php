<?php
declare(strict_types=1);

namespace SkyDiablo\DhcpServer\DHCPPacket\Options;

use SkyDiablo\DhcpServer\DHCPPacket\Options\Serializer\OptionSerializerInterface;

class Dumper
{

    protected OptionSerializerInterface $serializer;

    /**
     * @param OptionSerializerInterface $serializer
     */
    public function __construct(OptionSerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }


    public function dump(array $options): string
    {
        return array_reduce($options, function (string $carry, OptionInterface $option) {
            return $carry . $this->serializer->serialize($option);
        }, '');
    }

}