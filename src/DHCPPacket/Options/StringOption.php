<?php
declare(strict_types=1);

namespace SkyDiablo\DhcpServer\DHCPPacket\Options;

class StringOption extends AbstractOption
{

    protected string $value;

    public function __construct(int $code, string $value)
    {
        parent::__construct($code);
        $this->value = $value;
    }

    public function getValue(): string
    {
        return $this->value;
    }
}