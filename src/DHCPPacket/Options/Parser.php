<?php
declare(strict_types=1);

namespace SkyDiablo\DhcpServer\DHCPPacket\Options;

use SkyDiablo\DhcpServer\DHCPPacket\Options\Serializer\OptionSerializerInterface;

class Parser
{

    const DEFAULT_HEADER_SIZE = 2;

    protected bool $flatOptions = false;
    protected bool $exposeRawOptions = false;
    protected OptionSerializerInterface $optionSerializerManager;

    /**
     * @param bool $flatOptions
     * @param bool $exposeUnknownOptions
     * @param bool $forceRawOption
     * @param OptionSerializerInterface $optionSerializerManager
     */
    public function __construct(
        OptionSerializerInterface $optionSerializerManager,
        bool                      $flatOptions = false,
        bool                      $exposeUnknownOptions = false
    )
    {
        $this->flatOptions = $flatOptions;
        $this->exposeRawOptions = $exposeUnknownOptions;
        $this->optionSerializerManager = $optionSerializerManager;
    }


    /**
     * @param string $data binary data
     * @return OptionInterface[]
     */
    public function parse(string $data): array
    {
        $options = [];
        while ($data) {
            $rawOption = $this->parseToRawOption($data);
            switch (true) {
                case $rawOption->getCode() === OptionInterface::PAD:
                    // PAD is a special case, it is not an Option, but a padding
                    $rawOption = new RawOption(
                        OptionInterface::PAD,
                        (self::DEFAULT_HEADER_SIZE * -1) + 1,
                        ''
                    );
                    break;
                case $rawOption->getCode() === OptionInterface::END:
                    break 2; // stop parsing
                default:
                    if ($option = $this->parseRawOption($rawOption)) {
                        if (($option instanceof MultiOption) && $this->flatOptions) {
                            $options = array_merge($options, $option->getValue());
                        } else {
                            $options[] = $option;
                        }
                    } elseif ($this->exposeRawOptions) {
                        $options[] = $rawOption;
                    }
            }
            $data = substr($data, $rawOption->getLength() + self::DEFAULT_HEADER_SIZE); // reduce payload
        }
        return $options;
    }

    protected function parseToRawOption(string $data): RawOption
    {
        static $format = 'C' . self::DEFAULT_HEADER_SIZE;
        list(, $code, $length) = unpack($format, $data); // parse the header (code and length)
        $value = substr($data, self::DEFAULT_HEADER_SIZE, $length); // copy value except header (code and length)
        return new RawOption($code, $length, $value);
    }

    /**
     * @param RawOption $rawOption
     * @return OptionInterface|null
     */
    protected function parseRawOption(RawOption $rawOption): ?OptionInterface
    {
        /** @var OptionSerializerInterface $optionSerializer */
        try {
            return $this->optionSerializerManager->deserialize($rawOption);
        } catch (\Exception $e) {
            //TODO: log, event?
        }
        return null;
    }


}