<?php
declare(strict_types=1);

namespace SkyDiablo\DhcpServer\DHCPPacket\Options;

/**
 * Interface OptionInterface
 * @package SkyDiablo\DhcpServer\DHCPPacket\Options
 */
interface OptionInterface
{

    // TODO: add constants for option types, see: https://kea.readthedocs.io/en/kea-1.6.2/arm/dhcp4-srv.html#id2
    // RFC 2132
    // https://tools.ietf.org/html/rfc2132
    const PAD = 0;
    const END = 255;
    const SubnetMask = 1;
    const TimeOffset = 2;
    const Router = 3;
    const TimeServer = 4;
    const NameServer = 5;
    const DomainNameServer = 6;
    const LogServer = 7;
    const CookieServer = 8;
    const LPRServer = 9;
    const ImpressServer = 10;
    const ResourceLocationServer = 11;
    const HostName = 12;
    const BootFileSize = 13;
    const MeritDumpFile = 14;
    const DomainName = 15;
    const SwapServer = 16;
    const RootPath = 17;
    const ExtensionsPath = 18;
    const IPForwardingEnable = 19;
    const NonLocalSourceRoutingEnable = 20;
    const PolicyFilter = 21;
    const MaximumDatagramReassemblySize = 22;
    const DefaultIPTimeToLive = 23;
    const PathMTUAgingTimeout = 24;
    const PathMTUPlateauTable = 25;
    const InterfaceMTU = 26;
    const AllSubnetsAreLocal = 27;
    const BroadcastAddress = 28;
    const PerformMaskDiscovery = 29;
    const MaskSupplier = 30;
    const PerformRouterDiscovery = 31;
    const RouterSolicitationAddress = 32;
    const StaticRoute = 33;
    const TrailerEncapsulation = 34;
    const ARPCacheTimeout = 35;
    const EthernetEncapsulation = 36;
    const TCPDefaultTTL = 37;
    const TCPKeepaliveInterval = 38;
    const TCPKeepaliveGarbage = 39;
    const NetworkInformationServiceDomain = 40;
    const NetworkInformationServiceServers = 41;
    const NetworkTimeProtocolServers = 42;
    const VendorSpecificInformation = 43;
    const NetBIOSOverTCPIPNameServer = 44;
    const NetBIOSOverTCPIPDatagramDistributionServer = 45;
    const NetBIOSOverTCPIPNodeType = 46;
    const NetBIOSOverTCPIPScope = 47;
    const XWindowSystemFontServer = 48;
    const XWindowSystemDisplayManager = 49;
    const RequestedIPAddress = 50;
    const IPAddressLeaseTime = 51;
    const OptionOverload = 52;
    const MessageType = 53;
    const ServerIdentifier = 54;
    const ParameterRequestList = 55;
    const Message = 56;
    const MaximumDHCPMessageSize = 57;
    const RenewalTimeValue = 58;
    const RebindingTimeValue = 59;
    const VendorClassIdentifier = 60;
    const ClientIdentifier = 61; //@see: https://datatracker.ietf.org/doc/html/rfc6842

    public function getCode(): int;

    public function getValue(): mixed;

}