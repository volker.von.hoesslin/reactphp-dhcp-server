<?php
declare(strict_types=1);

namespace SkyDiablo\DhcpServer\DHCPPacket\Options;

use SkyDiablo\DhcpServer\DHCPPacket\Options\Enum\MessageType;

class MessageTypeOption extends AbstractOption
{

    protected MessageType $messageType;

    public function __construct(MessageType $messageType)
    {
        parent::__construct(OptionInterface::MessageType);
        $this->messageType = $messageType;
    }

    public function getValue(): MessageType
    {
        return $this->messageType;
    }
}