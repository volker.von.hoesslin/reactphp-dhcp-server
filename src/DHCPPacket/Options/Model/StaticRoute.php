<?php
declare(strict_types=1);

namespace SkyDiablo\DhcpServer\DHCPPacket\Options\Model;

use Darsyn\IP\Version\IPv4;

class StaticRoute
{

    protected IPv4 $destination;
    protected IPv4 $router;

    public function __construct(IPv4 $destination, IPv4 $router)
    {
        $this->destination = $destination;
        $this->router = $router;
    }

    public function getDestination(): IPv4
    {
        return $this->destination;
    }

    public function getRouter(): IPv4
    {
        return $this->router;
    }

}