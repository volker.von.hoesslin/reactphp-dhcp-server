<?php
declare(strict_types=1);

namespace SkyDiablo\DhcpServer\DHCPPacket\Options\Model;

use Darsyn\IP\Version\IPv4;

class PolicyFilter
{
    protected IPv4 $address;
    protected IPv4 $mask;

    public function __construct(IPv4 $address, IPv4 $mask)
    {
        $this->address = $address;
        $this->mask = $mask;
    }

    public function getAddress(): IPv4
    {
        return $this->address;
    }

    public function getMask(): IPv4
    {
        return $this->mask;
    }

    public function getCidr(): int
    {
        $long = ip2long($this->mask->getDotAddress());
        $base = 0xffffffff; // 4294967295 = 255.255.255.255
        return (int)(32 - log(($long ^ $base) + 1, 2));
    }

}