<?php
declare(strict_types=1);

namespace SkyDiablo\DhcpServer\DHCPPacket\Options\Model;

class ClientIdentifier
{

    protected int $type;
    protected string $identifier; // as hex string

    public function __construct(int $type, string $identifier)
    {
        $this->type = $type;
        $this->identifier = $identifier;
    }

    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @return string as hex string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

}