<?php
declare(strict_types=1);

namespace SkyDiablo\DhcpServer\DHCPPacket\Options;

use SkyDiablo\DhcpServer\Exception\InvalidArgumentException;

class RawOption extends AbstractOption
{

    protected string $value;
    protected int $length;

    /**
     * @param int $code
     * @param string $value
     * @param int $length
     */
    public function __construct(int $code, int $length, string $value)
    {
        if ($code < 0 || $code > 255) {
            throw new InvalidArgumentException('Option code must be between 0 and 255');
        }
        if ($length < 0 || $length > 255) {
            throw new InvalidArgumentException('Length must be between 0 and 255'); 
        }
        if (strlen($value) !== $length) {
            throw new InvalidArgumentException(
                sprintf('Value length (%d) does not match specified length (%d)', strlen($value), $length)
            );
        }
        
        parent::__construct($code);
        $this->value = $value;
        $this->length = $length;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @return int
     */
    public function getLength(): int
    {
        return $this->length;
    }

}