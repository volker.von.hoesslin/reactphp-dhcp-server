<?php
declare(strict_types=1);

namespace SkyDiablo\DhcpServer\DHCPPacket\Options;

use SkyDiablo\DhcpServer\DHCPPacket\Options\Model\ClientIdentifier;

class ClientIdentifierOption extends AbstractOption
{

    protected ClientIdentifier $clientIdentifier;

    public function __construct(int $code, ClientIdentifier $value)
    {
        parent::__construct($code);
        $this->clientIdentifier = $value;
    }

    public function getValue(): ClientIdentifier
    {
        return $this->clientIdentifier;
    }

    public function __clone(): void
    {
        $this->clientIdentifier = clone $this->clientIdentifier;
    }


}