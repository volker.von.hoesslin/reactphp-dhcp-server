<?php
declare(strict_types=1);

namespace SkyDiablo\DhcpServer\DHCPPacket\Options;

class IntOption extends AbstractOption
{

    protected int $value;

    public function __construct(int $code, int $value)
    {
        parent::__construct($code);
        $this->value = $value;
    }

    public function getValue(): int
    {
        return $this->value;
    }
}