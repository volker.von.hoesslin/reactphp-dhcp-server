<?php
declare(strict_types=1);

namespace SkyDiablo\DhcpServer\DHCPPacket\Options\Serializer;

use SkyDiablo\DhcpServer\DHCPPacket\Options\OptionInterface;
use SkyDiablo\DhcpServer\DHCPPacket\Options\RawOption;

interface OptionSerializerInterface
{

    /**
     * Serialize option to raw data for sending included option header (code and length)
     * @param OptionInterface $option
     * @return string
     */
    public function serialize(OptionInterface $option): string;

    /**
     * @param RawOption $rawOption
     * @return OptionInterface
     */
    public function deserialize(RawOption $rawOption): OptionInterface;
}