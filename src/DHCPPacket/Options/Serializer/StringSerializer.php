<?php
declare(strict_types=1);

namespace SkyDiablo\DhcpServer\DHCPPacket\Options\Serializer;

use SkyDiablo\DhcpServer\DHCPPacket\Options\OptionInterface;
use SkyDiablo\DhcpServer\DHCPPacket\Options\RawOption;
use SkyDiablo\DhcpServer\DHCPPacket\Options\StringOption;

class StringSerializer implements OptionSerializerInterface
{
    /**
     * @param OptionInterface $option
     * @return string
     */
    public function serialize(OptionInterface $option): string
    {
        $value = substr($option->getValue(), 0, 255); // 255 is max length of string option
        return pack('C2', $option->getCode(), strlen($value)) . $value;
    }

    public function deserialize(RawOption $rawOption): OptionInterface
    {
        return new StringOption($rawOption->getCode(), $rawOption->getValue());
    }


}