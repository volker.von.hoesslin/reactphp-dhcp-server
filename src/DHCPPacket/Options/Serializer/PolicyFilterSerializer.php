<?php
declare(strict_types=1);

namespace SkyDiablo\DhcpServer\DHCPPacket\Options\Serializer;

use Darsyn\IP\Version\IPv4;
use SkyDiablo\DhcpServer\DHCPPacket\Options\Model\PolicyFilter;
use SkyDiablo\DhcpServer\DHCPPacket\Options\MultiOption;
use SkyDiablo\DhcpServer\DHCPPacket\Options\OptionInterface;
use SkyDiablo\DhcpServer\DHCPPacket\Options\RawOption;

class PolicyFilterSerializer implements OptionSerializerInterface
{

    public function serialize(OptionInterface $option): string
    {
        if ($option instanceof MultiOption) {
            $values = $option->getValue();
        } else {
            $values = [$option->getValue()];
        }
        $payload = '';
        /** @var PolicyFilter $policyFilter */
        foreach ($values as $policyFilter) {
            $payload .= $policyFilter->getAddress()->getBinary() . $policyFilter->getMask()->getBinary();
        }
        $header = pack('C2', $option->getCode(), strlen($payload));
        return $header . $payload;
    }

    public function deserialize(RawOption $rawOption): OptionInterface
    {
        $options = [];
        $value = $rawOption->getValue();
        for ($i = 0, $max = $rawOption->getLength() / 8; $i < $max; $i += 8) {
            $options[] = new PolicyFilter(IPv4::factory(substr($value, $i, 4)), IPv4::factory(substr($value, $i + 4, 4)));
        }
        return new MultiOption($rawOption->getCode(), $options);
    }
}