<?php
declare(strict_types=1);

namespace SkyDiablo\DhcpServer\DHCPPacket\Options\Serializer;

use SkyDiablo\DhcpServer\DHCPPacket\Options\OptionInterface;
use SkyDiablo\DhcpServer\DHCPPacket\Options\RawOption;
use SkyDiablo\DhcpServer\DHCPPacket\Options\VendorOption;
use SkyDiablo\DhcpServer\Exception\InvalidArgumentException;

class VendorManager implements OptionSerializerInterface
{

    public function serialize(OptionInterface $option): string
    {
        if (!$option instanceof VendorOption) {
            throw new InvalidArgumentException('VendorManager can only serialize VendorOption');
        }

        $payload = substr($option->getValue(), 0, 255); // 255 is max length of value
        return pack('C2', $option->getCode(), strlen($payload)) . $payload;
    }

    public function deserialize(RawOption $rawOption): OptionInterface
    {
        return new VendorOption($rawOption->getCode(), $rawOption->getLength(), $rawOption->getValue());
    }
}