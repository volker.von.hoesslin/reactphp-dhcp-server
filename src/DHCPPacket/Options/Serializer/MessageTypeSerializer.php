<?php
declare(strict_types=1);

namespace SkyDiablo\DhcpServer\DHCPPacket\Options\Serializer;

use SkyDiablo\DhcpServer\DHCPPacket\Options\Enum\MessageType;
use SkyDiablo\DhcpServer\DHCPPacket\Options\IntOption;
use SkyDiablo\DhcpServer\DHCPPacket\Options\MessageTypeOption;
use SkyDiablo\DhcpServer\DHCPPacket\Options\OptionInterface;
use SkyDiablo\DhcpServer\DHCPPacket\Options\RawOption;

class MessageTypeSerializer extends IntSerializer
{

    public function __construct()
    {
        parent::__construct(self::BIT_8, false);
    }

    /**
     * @param MessageTypeOption $option
     * @return string
     */
    public function serialize(OptionInterface $option): string
    {
        return parent::serialize(new IntOption(
            $option->getCode(),
            $option->getValue()->value
        ));
    }


    public function deserialize(RawOption $rawOption): OptionInterface
    {
        $option = parent::deserialize($rawOption);
        if ($messageType = MessageType::tryFrom($option->getValue())) {
            return new MessageTypeOption($messageType);
        } else {
            throw new \RuntimeException('Invalid message type');
        }
    }
}