<?php
declare(strict_types=1);

namespace SkyDiablo\DhcpServer\DHCPPacket\Options\Serializer;

use SkyDiablo\DhcpServer\DHCPPacket\Options\ClientIdentifierOption;
use SkyDiablo\DhcpServer\DHCPPacket\Options\Model\ClientIdentifier;
use SkyDiablo\DhcpServer\DHCPPacket\Options\OptionInterface;
use SkyDiablo\DhcpServer\DHCPPacket\Options\RawOption;

class ClientIdentifierSerializer implements OptionSerializerInterface
{

    public function serialize(OptionInterface $option): string
    {
        return pack('C2H*', $option->getCode(), strlen($option->getValue()) / 2, $option->getValue());
    }

    public function deserialize(RawOption $rawOption): OptionInterface
    {
        //TODO: check if the length is correct
        $clientIdentifier = new ClientIdentifier(
            unpack('C1', $rawOption->getValue())[1],
            unpack('H*', substr($rawOption->getValue(), 1))[1] //skip first byte
        );
        return new ClientIdentifierOption($rawOption->getCode(), $clientIdentifier);
    }
}