<?php
declare(strict_types=1);

namespace SkyDiablo\DhcpServer\DHCPPacket\Options\Serializer;

use SkyDiablo\DhcpServer\DHCPPacket\Options\OptionInterface;
use SkyDiablo\DhcpServer\DHCPPacket\Options\RawOption;

class Manager implements OptionSerializerInterface
{

    /**
     * @var array|OptionSerializerInterface[] $serializers
     */
    protected array $serializers = [];

    public function __construct()
    {
        $this->init();
    }

    /**
     * Initialize the serializers for the options in the packet for RFC 2132
     * @see https://tools.ietf.org/html/rfc2132
     * @see https://datatracker.ietf.org/doc/html/rfc1700#page-88
     * @return void
     */
    protected function init()
    {
        // aggregation helper function
        $initializer = function (array $dhcpOptions, OptionSerializerInterface $serializer) {
            foreach ($dhcpOptions as $dhcpOptionCode) {
                $this->registerSerializer($dhcpOptionCode, $serializer);
            }
        };

        //Vendor Specific Information
        $this->registerSerializer(OptionInterface::VendorSpecificInformation, new VendorSerializer());

        // Client Identifier
        $this->registerSerializer(OptionInterface::ClientIdentifier, new ClientIdentifierSerializer());

        // Policy Filter: https://datatracker.ietf.org/doc/html/rfc2132#section-4.3
        $this->registerSerializer(OptionInterface::PolicyFilter, new PolicyFilterSerializer());

        // Static Route: https://datatracker.ietf.org/doc/html/rfc2132#section-5.8
        $this->registerSerializer(OptionInterface::StaticRoute, new StaticRouteSerializer());

        // Message Type: https://datatracker.ietf.org/doc/html/rfc1533#section-9.4
        $this->registerSerializer(OptionInterface::MessageType, new MessageTypeSerializer());

        //IPv4
        $initializer([
            OptionInterface::SubnetMask,
            OptionInterface::SwapServer,
            OptionInterface::BroadcastAddress,
            OptionInterface::RouterSolicitationAddress,
            OptionInterface::RequestedIPAddress,
            OptionInterface::ServerIdentifier,
        ], new IPv4Serializer(false));

        //INT32 - non MultiMode
        $initializer([
            OptionInterface::TimeOffset,
            OptionInterface::PathMTUAgingTimeout,
            OptionInterface::ARPCacheTimeout,
            OptionInterface::TCPKeepaliveInterval,
            OptionInterface::IPAddressLeaseTime,
            OptionInterface::RenewalTimeValue,
            OptionInterface::RebindingTimeValue,
        ], new IntSerializer(IntSerializer::BIT_32, false));

        //INT16 - non MultiMode
        $initializer([
            OptionInterface::BootFileSize,
            OptionInterface::MaximumDatagramReassemblySize, //INFO: The minimum value legal value is 576. //TODO: add validator?
            OptionInterface::InterfaceMTU,
            OptionInterface::MaximumDHCPMessageSize,
        ], new IntSerializer(IntSerializer::BIT_16, false));

        //INT16 - MultiMode
        $initializer([
            OptionInterface::PathMTUPlateauTable,
        ], new IntSerializer(IntSerializer::BIT_16, true));

        //INT8 - non MultiMode
        $initializer([
            OptionInterface::DefaultIPTimeToLive,
            OptionInterface::TCPDefaultTTL,
            OptionInterface::NetBIOSOverTCPIPNodeType,
            OptionInterface::OptionOverload, //@see: https://datatracker.ietf.org/doc/html/rfc1533#section-9.3
        ], new IntSerializer(IntSerializer::BIT_8, false));

        //INT8 - MultiMode
        $initializer([
            OptionInterface::ParameterRequestList,
        ], new IntSerializer(IntSerializer::BIT_8, true));

        //Multi IPv4
        $initializer([
            OptionInterface::Router,
            OptionInterface::TimeServer,
            OptionInterface::NameServer,
            OptionInterface::DomainNameServer,
            OptionInterface::LogServer,
            OptionInterface::CookieServer,
            OptionInterface::LPRServer,
            OptionInterface::ImpressServer,
            OptionInterface::ResourceLocationServer,
            OptionInterface::NetworkInformationServiceServers,
            OptionInterface::NetworkTimeProtocolServers,
            OptionInterface::NetBIOSOverTCPIPNameServer,
            OptionInterface::NetBIOSOverTCPIPDatagramDistributionServer,
            OptionInterface::XWindowSystemFontServer,
            OptionInterface::XWindowSystemDisplayManager,
        ], new IPv4Serializer(true));

        //String
        $initializer([
            OptionInterface::HostName,
            OptionInterface::MeritDumpFile,
            OptionInterface::DomainName,
            OptionInterface::RootPath,
            OptionInterface::ExtensionsPath,
            OptionInterface::NetworkInformationServiceDomain,
            OptionInterface::NetBIOSOverTCPIPScope, //TODO: check if this is correct
            OptionInterface::Message,
            OptionInterface::VendorClassIdentifier,
        ], new StringSerializer());

        //Boolean
        $initializer([
            OptionInterface::IPForwardingEnable,
            OptionInterface::NonLocalSourceRoutingEnable,
            OptionInterface::AllSubnetsAreLocal,
            OptionInterface::PerformMaskDiscovery,
            OptionInterface::MaskSupplier,
            OptionInterface::PerformRouterDiscovery,
            OptionInterface::TrailerEncapsulation,
            OptionInterface::EthernetEncapsulation,
            OptionInterface::TCPKeepaliveGarbage,
        ], new BooleanSerializer());

    }

    public function registerSerializer(int $dhcpOptionCode, OptionSerializerInterface $serializer)
    {
        $this->serializers[$dhcpOptionCode] = $serializer;
    }

    /**
     * @param RawOption $rawOption
     * @return OptionInterface
     */
    public function deserialize(RawOption $rawOption): OptionInterface
    {
        if (!isset($this->serializers[$rawOption->getCode()])) {
            throw new \RuntimeException(sprintf('No serializer for option %s', $rawOption->getCode()));
        }
        return $this->serializers[$rawOption->getCode()]->deserialize($rawOption);
    }

    /**
     * @param OptionInterface $option
     * @return string
     */
    public function serialize(OptionInterface $option): string
    {
        $serializer = $this->serializers[$option->getCode()] ?? null;
        if ($serializer instanceof OptionSerializerInterface) {
            return $serializer->serialize($option);
        }
        throw new \RuntimeException(sprintf('No serializer for option "%s" found', $option->getCode()));
    }

}