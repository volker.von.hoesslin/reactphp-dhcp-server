<?php
declare(strict_types=1);

namespace SkyDiablo\DhcpServer\DHCPPacket\Options\Serializer;

use SkyDiablo\DhcpServer\DHCPPacket\Options\BooleanOption;
use SkyDiablo\DhcpServer\DHCPPacket\Options\OptionInterface;
use SkyDiablo\DhcpServer\DHCPPacket\Options\RawOption;

class BooleanSerializer implements OptionSerializerInterface
{

    /**
     * @param OptionInterface|BooleanOption $option
     * @return string
     */
    public function serialize(OptionInterface $option): string
    {
        return pack('C3', $option->getCode(), 1, (int)$option->getValue());
    }

    public function deserialize(RawOption $rawOption): OptionInterface
    {
        return new BooleanOption($rawOption->getCode(), (int)($rawOption->getValue()[0] ?? 0) === 1);
    }
}