<?php
declare(strict_types=1);

namespace SkyDiablo\DhcpServer\DHCPPacket\Options\Serializer;

use SkyDiablo\DhcpServer\DHCPPacket\Options\IntOption;
use SkyDiablo\DhcpServer\DHCPPacket\Options\MultiOption;
use SkyDiablo\DhcpServer\DHCPPacket\Options\OptionInterface;
use SkyDiablo\DhcpServer\DHCPPacket\Options\RawOption;

class IntSerializer implements OptionSerializerInterface
{
    const BIT_32 = 'N*';
    const BIT_16 = 'n*';
    const BIT_8 = 'C*';

    protected string $format;
    protected bool $multiMode;

    /**
     * @param string $format
     * @param bool $multiMode
     */
    public function __construct(string $format, bool $multiMode = false)
    {
        $this->format = $format;
        $this->multiMode = $multiMode;
    }

    public function serialize(OptionInterface $option): string
    {
        if ($option instanceof MultiOption) {
            $values = array_map(function (IntOption $option) {
                return $option->getValue();
            }, $option->getValue());
        } else {
            $values = [$option->getValue()];
        }
        $payload = pack($this->format, ...$values);
        $header = pack('C2', $option->getCode(), strlen($payload));
        return $header . $payload;
    }

    public function deserialize(RawOption $rawOption): OptionInterface
    {
        $options = array_map(function (int $int) use ($rawOption) {
            return new IntOption(
                $rawOption->getCode(),
                $int
            );
        }, unpack($this->format, $rawOption->getValue()));
        return $this->multiMode ? new MultiOption($rawOption->getCode(), $options) : current($options);
    }
}