<?php
declare(strict_types=1);

namespace SkyDiablo\DhcpServer\DHCPPacket\Options\Serializer;

use SkyDiablo\DhcpServer\DHCPPacket\Options\Dumper;
use SkyDiablo\DhcpServer\DHCPPacket\Options\MultiOption;
use SkyDiablo\DhcpServer\DHCPPacket\Options\OptionInterface;
use SkyDiablo\DhcpServer\DHCPPacket\Options\Parser;
use SkyDiablo\DhcpServer\DHCPPacket\Options\RawOption;
use SkyDiablo\DhcpServer\Exception\InvalidArgumentException;

class VendorSerializer implements OptionSerializerInterface
{

    protected Parser $parser;
    protected Dumper $dumper;

    /**
     * @param Parser|null $parser
     * @param Dumper|null $dumper
     */
    public function __construct(?Parser $parser = null, ?Dumper $dumper = null)
    {
        $this->parser = $parser ?? new Parser(new VendorManager());
        $this->dumper = $dumper ?? new Dumper(new VendorManager());
    }


    /**
     * @param OptionInterface $option
     * @return string
     */
    public function serialize(OptionInterface $option): string
    {
        if ($option instanceof MultiOption) {
            $options = $option->getValue();
        } else {
            $options = [$option];
        }

        $payload = $this->dumper->dump($options);
        $payloadLength = strlen($payload);
        if ($payloadLength > 255) {
            throw new InvalidArgumentException('Vendor option payload is too long, given %d, max %d', $payloadLength, 255);
        }
        return pack('C2', OptionInterface::VendorSpecificInformation,) . $payload;
    }

    public function deserialize(RawOption $rawOption): OptionInterface
    {
        return new MultiOption($rawOption->getCode(), $this->parser->parse($rawOption->getValue()));
    }

}