<?php
declare(strict_types=1);

namespace SkyDiablo\DhcpServer\DHCPPacket\Options\Serializer;

use Darsyn\IP\Version\IPv4;
use SkyDiablo\DhcpServer\DHCPPacket\Options\IPv4Option;
use SkyDiablo\DhcpServer\DHCPPacket\Options\MultiOption;
use SkyDiablo\DhcpServer\DHCPPacket\Options\OptionInterface;
use SkyDiablo\DhcpServer\DHCPPacket\Options\RawOption;
use SkyDiablo\DhcpServer\Exception\InvalidArgumentException;

class IPv4Serializer implements OptionSerializerInterface
{
    protected bool $multiMode = false;

    /**
     * @param bool $multiMode
     */
    public function __construct(bool $multiMode = false)
    {
        $this->multiMode = $multiMode;
    }

    /**
     * @param OptionInterface $option
     * @return string
     */
    public function serialize(OptionInterface $option): string
    {
        $values = [];
        $options = $option instanceof MultiOption ? $option->getValue() : [$option];
        foreach ($options as $option) {
            if($option instanceof IPv4Option) {
                $values[] = $option->getValue()->getBinary();
            } else {
                throw new InvalidArgumentException(sprintf('Option must be of type %s or %s, %s was provided', IPv4Option::class, MultiOption::class, get_class($option)));
            }
        }
        $payload = implode('', $values);
        $header = pack('C2', $option->getCode(), strlen($payload));
        return $header . $payload;
    }

    public function deserialize(RawOption $rawOption): OptionInterface
    {
        $options = array_map(function (int $rawIp) use ($rawOption) {
            return new IPv4Option(
                $rawOption->getCode(),
                IPv4::factory(long2ip($rawIp))
            );
        }, unpack('N*', $rawOption->getValue()) ?: [0]);
        return $this->multiMode ? new MultiOption($rawOption->getCode(), $options) : array_shift($options);
    }
}