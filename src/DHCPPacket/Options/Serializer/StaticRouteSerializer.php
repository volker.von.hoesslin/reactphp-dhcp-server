<?php
declare(strict_types=1);

namespace SkyDiablo\DhcpServer\DHCPPacket\Options\Serializer;

use Darsyn\IP\Version\IPv4;
use SkyDiablo\DhcpServer\DHCPPacket\Options\Model\StaticRoute;
use SkyDiablo\DhcpServer\DHCPPacket\Options\MultiOption;
use SkyDiablo\DhcpServer\DHCPPacket\Options\OptionInterface;
use SkyDiablo\DhcpServer\DHCPPacket\Options\RawOption;

class StaticRouteSerializer implements OptionSerializerInterface
{

    public function serialize(OptionInterface $option): string
    {
        if ($option instanceof MultiOption) {
            $values = $option->getValue();
        } else {
            $values = [$option->getValue()];
        }
        $payload = '';
        /** @var StaticRoute $staticRoute */
        foreach ($values as $staticRoute) {
            $payload .= $staticRoute->getDestination()->getBinary() . $staticRoute->getRouter()->getBinary();
        }
        $header = pack('C2', $option->getCode(), strlen($payload));
        return $header . $payload;
    }

    public function deserialize(RawOption $rawOption): OptionInterface
    {
        $options = [];
        $value = $rawOption->getValue();
        for ($i = 0, $max = $rawOption->getLength() / 8; $i < $max; $i += 8) {
            $options[] = new StaticRoute(IPv4::factory(substr($value, $i, 4)), IPv4::factory(substr($value, $i + 4, 4)));
        }
        return new MultiOption($rawOption->getCode(), $options);
    }
}