<?php
declare(strict_types=1);

namespace SkyDiablo\DhcpServer\DHCPPacket\Options;

class MultiOption extends AbstractOption
{

    protected array $options;

    /**
     * @param OptionInterface[] $options
     */
    public function __construct(int $code, array $options)
    {
        parent::__construct($code);
        $this->options = $options;
    }

    /**
     * @return OptionInterface[]
     */
    public function getValue(): array
    {
        return $this->options;
    }

    public function __clone(): void
    {
        $this->options = array_map(function (OptionInterface $option) {
            return clone $option;
        }, $this->options);
    }


}