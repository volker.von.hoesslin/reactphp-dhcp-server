<?php
declare(strict_types=1);

namespace SkyDiablo\DhcpServer\DHCPPacket\Options;

use SkyDiablo\DhcpServer\DHCPPacket\Options\Model\PolicyFilter;

class PolicyFilterOption extends AbstractOption
{

    protected PolicyFilter $policyFilter;

    public function __construct(int $code, PolicyFilter $value)
    {
        parent::__construct($code);
        $this->policyFilter = $value;
    }

    public function getValue(): PolicyFilter
    {
        return $this->policyFilter;
    }

    public function __clone(): void
    {
        $this->policyFilter = clone $this->policyFilter;
    }


}