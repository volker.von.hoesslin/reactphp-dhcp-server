<?php
declare(strict_types=1);

namespace SkyDiablo\DhcpServer\DHCPPacket\Options;

use SkyDiablo\DhcpServer\DHCPPacket\Options\Model\StaticRoute;

class StaticRouterOption extends AbstractOption
{

    protected StaticRoute $staticRoute;

    /**
     * @param StaticRoute $staticRoute
     */
    public function __construct(int $code, StaticRoute $staticRoute)
    {
        parent::__construct($code);
        $this->staticRoute = $staticRoute;
    }


    public function getValue(): StaticRoute
    {
        return $this->staticRoute;
    }

    public function __clone(): void
    {
        $this->staticRoute = clone $this->staticRoute;
    }


}