<?php
declare(strict_types=1);

namespace SkyDiablo\DhcpServer\DHCPPacket\Options\Enum;

enum MessageType: int
{
    case NOOP = 0; // No Operation
    case DHCPDISCOVER = 1;
    case DHCPOFFER = 2;
    case DHCPREQUEST = 3;
    case DHCPDECLINE = 4;
    case DHCPACK = 5;
    case DHCPNAK = 6;
    case DHCPRELEASE = 7;
    case DHCPINFORM = 8;
}