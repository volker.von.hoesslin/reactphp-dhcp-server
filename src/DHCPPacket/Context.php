<?php
declare(strict_types=1);

namespace SkyDiablo\DhcpServer\DHCPPacket;

use Psr\Http\Message\UriInterface;
use React\Datagram\SocketInterface;

class Context
{

    protected DHCPPacket $requestPacket;
    protected DHCPPacket $responsePacket;
    protected UriInterface $peer;
    protected SocketInterface $socket;

    /**
     * @param DHCPPacket $requestPacket
     * @param DHCPPacket $responsePacket
     * @param UriInterface $peer
     * @param SocketInterface $socket
     */
    public function __construct(DHCPPacket $requestPacket, DHCPPacket $responsePacket, UriInterface $peer, SocketInterface $socket)
    {
        $this->requestPacket = $requestPacket;
        $this->responsePacket = $responsePacket;
        $this->peer = $peer;
        $this->socket = $socket;
    }

    /**
     * @return DHCPPacket
     */
    public function getRequestPacket(): DHCPPacket
    {
        return $this->requestPacket;
    }

    /**
     * @return DHCPPacket
     */
    public function getResponsePacket(): DHCPPacket
    {
        return $this->responsePacket;
    }

    /**
     * @return UriInterface
     */
    public function getPeer(): UriInterface
    {
        return $this->peer;
    }

    /**
     * @return SocketInterface
     */
    public function getSocket(): SocketInterface
    {
        return $this->socket;
    }

}