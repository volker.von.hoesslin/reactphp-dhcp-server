<?php
declare(strict_types=1);

namespace SkyDiablo\DhcpServer\DHCPPacket;

use Darsyn\IP\Version\IPv4;
use SkyDiablo\DhcpServer\DHCPPacket\Options\Enum\MessageType;
use SkyDiablo\DhcpServer\DHCPPacket\Options\MessageTypeOption;
use SkyDiablo\DhcpServer\DHCPPacket\Options\MultiOption;
use SkyDiablo\DhcpServer\DHCPPacket\Options\OptionInterface;

enum DHCPMessageOpCode: int
{
    case BOOTREQUEST = 1;
    case BOOTREPLY = 2;
}

enum HardwareAddressType: int
{
    case ETHERNET = 1;
    case IEEE802 = 6;
    case ARCNET = 7;
    case SLIP = 28;
    case FDDI = 29;
    case PPP = 30;
    case FDDI_BITSWAPPED = 31;
    case SLIP_BITSWAPPED = 32;
    case PPP_BITSWAPPED = 33;
    case ATM_RFC1483 = 37;
    case LLC = 48;
    case RAW = 255;
    case IEEE802_TR = 26;
    case APPLETALK_DDP = 16;
    case ATM_CLIP = 35;
    case LOCALTALK = 39;
    case IEEE802_11 = 71;
    case FIBRE_CHANNEL = 79;
    case IEEE1394_FIREWIRE = 112;
    case IEEE802_R = 113;
    case FIBRE_CHANNEL_FC2 = 121;
    case IEEE802_W = 122;
    case IEEE802_P = 123;
    case IEEE802_4 = 124;
    case IEEE802_5 = 125;
    case IEEE802_9 = 126;
    case FIBRE_CHANNEL_FCWW = 127;
    case IEEE802_16_MAC_CPS = 138;
    case IEEE802_16_MAC_CPS_FH = 139;
    case IEEE802_16_MAC_CPS_OFDMA = 140;
    case IEEE802_16_MAC_CPS_OFDMA_FH = 141;
    case IEEE802_16_MAC_CPS_OFDMA_2_DOT_4 = 142;
    case IEEE802_16_MAC_CPS_OFDMA_2_DOT_4_FH = 143;
    case IEEE802_16_MAC_CPS_OFDMA_3_DOT_6 = 144;
    case IEEE802_16_MAC_CPS_OFDMA_3_DOT_6_FH = 145;
    case IEEE802_16_MAC_CPS_OFDMA_5_DOT_8 = 146;
    case IEEE802_16_MAC_CPS_OFDMA_5_DOT_8_FH = 147;
    case IEEE802_16_MAC_CPS_OFDMA_7_DOT_2 = 148;
    case IEEE802_16_MAC_CPS_OFDMA_7_DOT_2_FH = 149;
    case IEEE802_16_MAC_CPS_OFDMA_10_DOT_4 = 150;
    case IEEE802_16_MAC_CPS_OFDMA_10_DOT_4_FH = 151;
    case IEEE802_16_MAC_CPS_OFDMA_15_DOT_6 = 152;
    case IEEE802_16_MAC_CPS_OFDMA_15_DOT_6_FH = 153;
    case IEEE802_16_MAC_CPS_OFDMA_20_DOT_8 = 154;
    case IEEE802_16_MAC_CPS_OFDMA_20_DOT_8_FH = 155;
    case IEEE802_16_MAC_CPS_OFDMA_25_DOT_6 = 156;
    case IEEE802_16_MAC_CPS_OFDMA_25_DOT_6_FH = 157;
    case IEEE802_16_MAC_CPS_OFDMA_31_DOT_8 = 158;
    case IEEE802_16_MAC_CPS_OFDMA_31_DOT_8_FH = 159;
    case IEEE802_16_MAC_CPS_OFDMA_40_DOT_0 = 160;
    case IEEE802_16_MAC_CPS_OFDMA_40_DOT_0_FH = 161;
    case IEEE802_16_MAC_CPS_OFDMA_50_DOT_12 = 162;
    case IEEE802_16_MAC_CPS_OFDMA_50_DOT_12_FH = 163;
    case IEEE802_16_MAC_CPS_OFDMA_60_DOT_16 = 164;
    case IEEE802_16_MAC_CPS_OFDMA_60_DOT_16_FH = 165;
    case IEEE802_16_MAC_CPS_OFDMA_100_DOT_24 = 166;
    case IEEE802_16_MAC_CPS_OFDMA_100_DOT_24_FH = 167;
    case IEEE802_16_MAC_CPS_OFDMA_200_DOT_48 = 168;
    case IEEE802_16_MAC_CPS_OFDMA_200_DOT_48_FH = 169;
    case IEEE802_16_MAC_CPS_OFDMA_400_DOT_96 = 170;
    case IEEE802_16_MAC_CPS_OFDMA_400_DOT_96_FH = 171;
    case IEEE802_16_MAC_CPS_OFDMA_800_DOT_192 = 172;
    case IEEE802_16_MAC_CPS_OFDMA_800_DOT_192_FH = 173;
    case IEEE802_16_MAC_CPS_OFDMA_1600_DOT_384 = 174;
}

enum DHCPFlag: int
{
    case BROADCAST = 0b1000000000000000;
    case UNICAST = 0b0000000000000000;
}

class DHCPPacket
{

    const MAGIC_COOKIE = "\x63\x82\x53\x63"; //dec: 99, 130, 83, 99

    /**
     * Message op code / message type
     * @var DHCPMessageOpCode
     */
    protected DHCPMessageOpCode $op;

    /**
     * Hardware address type, see ARP section in "Assigned Numbers" RFC; e.g., '1' = 10mb ethernet.
     * @var HardwareAddressType
     */
    protected HardwareAddressType $htype;

    /**
     * Hardware address length
     * @var int
     */
    protected int $hlen;

    /**
     * @var int number of hops
     */
    protected int $hops;

    /**
     * Transaction ID, a random number chosen by the client, used by the client
     * and server to associate messages and responses between a client and a server.
     * @var int
     */
    protected int $xid;

    /**
     * @var int seconds elapsed since client began address acquisition or renewal process
     */
    protected int $secs;

    /**
     * Flags
     * @var array|DHCPFlag[]
     */
    protected array $flags;

    /**
     * Client IP address; only filled in if client is in BOUND, RENEW or REBINDING state and can respond
     * @var IPv4
     */
    protected IPv4 $ciaddr;

    /**
     * 'your' (client) IP address.
     * @var IPv4
     */
    protected IPv4 $yiaddr;

    /**
     * IP address of next server to use in bootstrap; returned in DHCPOFFER, DHCPACK by server.
     * @var IPv4
     */
    protected IPv4 $siaddr;

    /**
     * Relay agent IP address, used in booting via a relay agent.
     * @var IPv4
     */
    protected IPv4 $giaddr;

    /**
     * @var string client hardware address
     */
    protected string $chaddr;

    /**
     * @var string|null optional server host name, null terminated string
     */
    protected ?string $sname;

    /**
     * Boot file name, null terminated string; "generic" name or null
     * in DHCPDISCOVER, fully qualified directory-path name in DHCPOFFER.
     * @var string|null
     */
    protected ?string $file;

    /**
     * Optional parameters field.
     * @var array
     */
    protected array $options = [];

    public function __clone(): void
    {
        isset($this->ciaddr) ? ($this->ciaddr = clone $this->ciaddr) : null;
        isset($this->giaddr) ? ($this->giaddr = clone $this->giaddr) : null;
        isset($this->siaddr) ? ($this->siaddr = clone $this->siaddr) : null;
        isset($this->yiaddr) ? ($this->yiaddr = clone $this->yiaddr) : null;

        //deep clone
        $this->options = array_map(function ($option) {
            return clone $option;
        }, $this->options);
    }


    /**
     * @param DHCPMessageOpCode $op
     * @return self
     */
    public function setOp(DHCPMessageOpCode $op): self
    {
        $this->op = $op;
        return $this;
    }

    /**
     * @param HardwareAddressType $hardwareAddressType
     * @return self
     */
    public function setHtype(HardwareAddressType $hardwareAddressType): self
    {
        $this->htype = $hardwareAddressType;
        return $this;
    }

    /**
     * @param int $hlen
     * @return DHCPPacket
     */
    public function setHlen(int $hlen): self
    {
        $this->hlen = $hlen;
        return $this;
    }

    /**
     * @param int $hops
     * @return DHCPPacket
     */
    public function setHops(int $hops): self
    {
        $this->hops = $hops;
        return $this;
    }

    /**
     * @param int $xid
     * @return DHCPPacket
     */
    public function setXid(int $xid): self
    {
        $this->xid = $xid;
        return $this;
    }

    /**
     * @param int $secs
     * @return DHCPPacket
     */
    public function setSecs(int $secs): self
    {
        $this->secs = $secs;
        return $this;
    }

    /**
     * @param array $flags
     * @return DHCPPacket
     */
    public function setFlags(array $flags): self
    {
        $this->flags = $flags;
        return $this;
    }

    /**
     * @param IPv4 $ciaddr
     * @return DHCPPacket
     */
    public function setCiaddr(IPv4 $ciaddr): self
    {
        $this->ciaddr = $ciaddr;
        return $this;
    }

    /**
     * @param IPv4 $yiaddr
     * @return DHCPPacket
     */
    public function setYiaddr(IPv4 $yiaddr): self
    {
        $this->yiaddr = $yiaddr;
        return $this;
    }

    /**
     * @param IPv4 $siaddr
     * @return DHCPPacket
     */
    public function setSiaddr(IPv4 $siaddr): self
    {
        $this->siaddr = $siaddr;
        return $this;
    }

    /**
     * @param IPv4 $giaddr
     * @return DHCPPacket
     */
    public function setGiaddr(IPv4 $giaddr): self
    {
        $this->giaddr = $giaddr;
        return $this;
    }

    /**
     * @param string $chaddr
     * @return DHCPPacket
     */
    public function setChaddr(string $chaddr): self
    {
        $this->chaddr = $chaddr;
        return $this;
    }

    /**
     * @param string|null $sname
     * @return DHCPPacket
     */
    public function setSname(?string $sname): self
    {
        $this->sname = $sname;
        return $this;
    }

    /**
     * @param string|null $file
     * @return DHCPPacket
     */
    public function setFile(?string $file): self
    {
        $this->file = $file;
        return $this;
    }

    /**
     * @param array $options
     * @return DHCPPacket
     */
    public function setOptions(array $options): self
    {
        $this->options = $options;
        return $this;
    }

    /**
     * @return DHCPMessageOpCode
     */
    public function getOp(): DHCPMessageOpCode
    {
        return $this->op;
    }

    /**
     * @return HardwareAddressType
     */
    public function getHtype(): HardwareAddressType
    {
        return $this->htype;
    }

    /**
     * @return int
     */
    public function getHlen(): int
    {
        return $this->hlen;
    }

    /**
     * @return int
     */
    public function getHops(): int
    {
        return $this->hops;
    }

    /**
     * @return int
     */
    public function getXid(): int
    {
        return $this->xid;
    }

    /**
     * @return int
     */
    public function getSecs(): int
    {
        return $this->secs;
    }

    /**
     * @return array|DHCPFlag[]
     */
    public function getFlags(): array
    {
        return $this->flags;
    }

    /**
     * @return IPv4
     */
    public function getCiaddr(): IPv4
    {
        return $this->ciaddr;
    }

    /**
     * @return IPv4
     */
    public function getYiaddr(): IPv4
    {
        return $this->yiaddr;
    }

    /**
     * @return IPv4
     */
    public function getSiaddr(): IPv4
    {
        return $this->siaddr;
    }

    /**
     * @return IPv4
     */
    public function getGiaddr(): IPv4
    {
        return $this->giaddr;
    }

    /**
     * @return string
     */
    public function getChaddr(): string
    {
        return $this->chaddr;
    }

    /**
     * @return string|null
     */
    public function getSname(): ?string
    {
        return $this->sname;
    }

    /**
     * @return string|null
     */
    public function getFile(): ?string
    {
        return $this->file;
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * @return int The length of the packet options field
     */
    public function optionsCount(): int
    {
        return count($this->options);
    }

    public function selectOption(int $option): array
    {
        return array_filter($this->options, function (OptionInterface $opt) use ($option) {
            return $opt->getCode() === $option;
        });
    }

    public function removeOption(int $option): self
    {
        $this->options = array_filter($this->options, function (OptionInterface $opt) use ($option) {
            return $opt->getCode() !== $option;
        });
        return $this;
    }

    public function getMessageType(): ?MessageType
    {
        if ($options = $this->selectOption(OptionInterface::MessageType)) {
            /** @var MessageTypeOption $messageTypeOption */
            $messageTypeOption = current($options); //select first
            return $messageTypeOption->getValue();
        }
        return null;
    }

    public function setMessageType(MessageType $messageType): self
    {
        $this->removeOption(OptionInterface::MessageType);
        $this->addOption(new MessageTypeOption($messageType));
        return $this;
    }

    public function getAggregatedOptions(): array
    {
        $aggregatedOptions = [];
        foreach ($this->options as $option) {
            $selected = $this->selectOption($option->getCode());
            if (count($selected) > 1) {
                $aggregatedOptions[$option->getCode()] = new MultiOption($option->getCode(), $selected);
            } else {
                $aggregatedOptions[$option->getCode()] = current($selected);
            }
            $this->removeOption($option->getCode());
        }
        return $aggregatedOptions;
    }

    public function addOption(OptionInterface $param)
    {
        $this->options[] = $param;
    }


}