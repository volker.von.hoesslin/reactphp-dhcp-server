<?php
declare(strict_types=1);

namespace SkyDiablo\DhcpServer\DHCPPacket;

use Clue\Hexdump\Hexdump;
use SkyDiablo\DhcpServer\DHCPPacket\Options\Dumper as OptionDumper;
use SkyDiablo\DhcpServer\DHCPPacket\Options\Serializer\Manager;
use SkyDiablo\DhcpServer\Helper\UnPackInteger;

class Dumper
{

    use UnPackInteger;

    protected OptionDumper $optionDumper;

    public function __construct(OptionDumper $optionDumper = null)
    {
        $this->optionDumper = $optionDumper ?? new OptionDumper(new Manager());
    }

    public function dump(DHCPPacket $packet): string
    {
        $dump = $this->dumpHeader($packet);
//        echo (new Hexdump())->dump($dump);
        $dump .= $this->dumpOptions($packet);
        return $dump;
    }

    private function dumpHeader(DHCPPacket $packet): string
    {
        $dump = $this->packInt8($packet->getOp()->value);
        $dump .= $this->packInt8($packet->getHtype()->value);
        $dump .= $this->packInt8($packet->getHlen());
        $dump .= $this->packInt8($packet->getHops());
        $dump .= $this->packInt32($packet->getXid());
        $dump .= $this->packInt16($packet->getSecs());
        $dump .= $this->packInt16(array_reduce($packet->getFlags(), function (int $carry, DHCPFlag $item) {
            return $carry | $item->value;
        }, 0));
        $dump .= $packet->getCiaddr()->getBinary();
        $dump .= $packet->getYiaddr()->getBinary();
        $dump .= $packet->getSiaddr()->getBinary();
        $dump .= $packet->getGiaddr()->getBinary();
        $dump .= str_pad(pack('H*', $packet->getChaddr()), 16, "\x00", STR_PAD_RIGHT);
        $dump .= str_pad(substr($packet->getSname(), 0, 64), 64, "\x00", STR_PAD_RIGHT);
        $dump .= str_pad(substr($packet->getFile(), 0, 128), 128, "\x00", STR_PAD_RIGHT);
        return $dump;
    }

    private function dumpOptions(DHCPPacket $packet): string
    {
        $dump = '';
        if ($packet->optionsCount()) {
            $dump .= DHCPPacket::MAGIC_COOKIE;
            $dump .= $this->optionDumper->dump($packet->getAggregatedOptions());
        }
        return $dump;
    }
}
