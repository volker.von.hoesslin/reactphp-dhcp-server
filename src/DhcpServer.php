<?php
declare(strict_types=1);

namespace SkyDiablo\DhcpServer;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use Psr\Http\Message\UriInterface;
use React\Datagram\SocketInterface;
use RingCentral\Psr7\Uri;
use SkyDiablo\DhcpServer\DHCPPacket\Context;
use SkyDiablo\DhcpServer\DHCPPacket\DHCPMessageOpCode;
use SkyDiablo\DhcpServer\DHCPPacket\DHCPPacket;
use SkyDiablo\DhcpServer\DHCPPacket\Dumper;
use SkyDiablo\DhcpServer\DHCPPacket\Options\Enum\MessageType;
use SkyDiablo\DhcpServer\DHCPPacket\Options\MessageTypeOption;
use SkyDiablo\DhcpServer\DHCPPacket\Options\OptionInterface;
use SkyDiablo\DhcpServer\DHCPPacket\Parser;

enum DhcpServerState
{
    case INIT;
    case BOUND;
    case RENEWING;
    case REBINDING;
    case REBOOTING;
    case REQUESTING;
    case RELEASING;
    case INFORMING;
    case DECLINING;
    case CONFLICTING;
    case UNKNOWN;
}

enum DhcpServerEvent: string
{
    case RAW_PACKET_RECEIVED = 'raw_packet_received';
    case PACKET_RECEIVED = 'packet_received';
}

class DhcpServer extends EventEmitter
{

    protected Parser $parser;
    protected Dumper $dumper;

    public function __construct(protected SocketServer $socket)
    {
        $this->parser = new Parser();
        $this->dumper = new Dumper();
        $this->socket->on(ServerEvent::MESSAGE->value, function ($message, $peer, SocketInterface $socket) {
            $this->onMessage($message, new Uri('udp://' . $peer), $socket);
        });
        $this->forwardEvents($this->socket, $this, [ServerEvent::ERROR->value]);
    }

    /**
     * @param EventEmitterInterface $source
     * @param EventEmitterInterface $target
     * @param array|ServerEvent[] $events
     * @return void
     */
    protected function forwardEvents(EventEmitterInterface $source, EventEmitterInterface $target, array $events)
    {
        foreach ($events as $event) {
            $source->on($event, function () use ($event, $target) {
                $target->emit($event, \func_get_args());
            });
        }
    }

    protected function onMessage(string $message, UriInterface $peer, SocketInterface $socket): void
    {
        $this->emit(DhcpServerEvent::RAW_PACKET_RECEIVED->value, [$message, $peer, $socket]);
        try {
            $requestPacket = $this->parser->parse($message);
            $context = new Context($requestPacket, $this->createResponsePacket($requestPacket), $peer, $socket);
            $this->emit(DhcpServerEvent::PACKET_RECEIVED->value, [$context]);

            if ($context->getRequestPacket()->getMessageType() !== MessageType::NOOP) {
                $dump = $this->dumper->dump($context->getResponsePacket());
                $socket->send($dump, sprintf('%s:%d', $peer->getHost(), $peer->getPort()));
            }

        } catch (\Exception $e) {
            $this->emit(ServerEvent::ERROR->value, [$e]);
        }
    }

    protected function createResponsePacket(DHCPPacket $requestPacket): DHCPPacket
    {
        $responseOptions = $requestPacket->selectOption(OptionInterface::ClientIdentifier);

        switch ($requestPacket->getMessageType()) {
            case MessageType::DHCPDISCOVER:
                $responseOptions[] = new MessageTypeOption(MessageType::DHCPOFFER);
                break;
            case MessageType::DHCPREQUEST:
                $responseOptions[] = new MessageTypeOption(MessageType::NOOP); // DHCPACK has to set manually
                break;
        }

        //                [
//                    new DHCPMessageOption(DHCPMessageOption::SERVER_IDENTIFIER, $requestPacket->getOption(DHCPMessageOption::REQUESTED_IP_ADDRESS)->getValue()),
//                    new DHCPMessageOption(DHCPMessageOption::IP_ADDRESS_LEASE_TIME, 3600),
//                    new DHCPMessageOption(DHCPMessageOption::RENEWAL_TIME, 1800),
//                    new DHCPMessageOption(DHCPMessageOption::REBINDING_TIME, 1800),
//                    new DHCPMessageOption(DHCPMessageOption::SUBNET_MASK, '


        return (clone $requestPacket)
            ->setOp(DHCPMessageOpCode::BOOTREPLY)
            ->setOptions($responseOptions);
    }


}