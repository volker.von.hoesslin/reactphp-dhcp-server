<?php
declare(strict_types=1);

namespace SkyDiablo\DhcpServer;

use Evenement\EventEmitter;
use Evenement\EventEmitterInterface;
use React\Datagram\Factory as UdpSocketFactory;
use React\Datagram\SocketInterface;
use React\EventLoop\Loop;

enum ServerEvent: string
{
    case MESSAGE = 'message';
    case ERROR = 'error';
    case CLOSE = 'close';
}

class SocketServer extends EventEmitter
{

    const DEFAULT_PORT = 67;

    public function __construct(string $host, int $port = self::DEFAULT_PORT, string $interface = null, UdpSocketFactory $udpSocketFactory = null)
    {
        $udpSocketFactory ??= new UdpSocketFactory(Loop::get());

        $udpSocketFactory->createServer(sprintf('%s:%d', $host, $port), [
            SOL_SOCKET => [
                SO_REUSEADDR => 1,
                SO_BROADCAST => 1,
                SO_REUSEPORT => 1,
                SO_BINDTODEVICE => $interface, //unknown interface name will be ignored
            ]
        ])->then(function (SocketInterface $server) {
            $this->forwardEvents($server, $this, array_map(function (ServerEvent $event) {
                return $event->value;
            }, ServerEvent::cases()));
        })->otherwise(function (\Exception $e) {
            $this->emit(ServerEvent::ERROR->value, [$e]);
        });
    }

    /**
     * @param EventEmitterInterface $source
     * @param EventEmitterInterface $target
     * @param array|ServerEvent[] $events
     * @return void
     */
    protected function forwardEvents(EventEmitterInterface $source, EventEmitterInterface $target, array $events)
    {
        foreach ($events as $event) {
            $source->on($event, function () use ($event, $target) {
                $target->emit($event, \func_get_args());
            });
        }
    }


}