<?php

declare(strict_types=1);

namespace SkyDiablo\DhcpServer\Exception;

/**
 * Base exception class for all DHCP library exceptions
 */
class BaseException extends \Exception
{
} 