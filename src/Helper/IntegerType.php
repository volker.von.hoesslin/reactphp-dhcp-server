<?php
declare(strict_types=1);

namespace SkyDiablo\DhcpServer\Helper;


class IntegerType
{

    const MSB = 0; //Most Significant Byte
    const LSB = 1; //Least Significant Byte

    const BIT_8 = 8;
    const BIT_16 = 16;
    const BIT_32 = 32;
    const BIT_64 = 64;
    const BIT_8_SIGNED = -8;

    const FORMATTER = [
        self::MSB => [
            self::BIT_8 => 'C',
            self::BIT_8_SIGNED => 'c',
            self::BIT_16 => 'n',
            self::BIT_32 => 'N',
            self::BIT_64 => 'J',
        ],
        self::LSB => [
            self::BIT_8 => 'C',
            self::BIT_16 => 'v',
            self::BIT_32 => 'V',
            self::BIT_64 => 'P',
        ],
    ];


}