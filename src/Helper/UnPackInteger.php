<?php

namespace SkyDiablo\DhcpServer\Helper;

trait UnPackInteger
{
    /**
     * @param int $bit
     * @param string $data
     * @param int $startPos
     * @param int $byteOrder
     * @return int
     */
    private function unpackInt(int $bit, string $data, int $startPos = 0, int $byteOrder = IntegerType::MSB): int
    {
        $format = IntegerType::FORMATTER[$byteOrder][$bit] ?? null;
        $payload = substr($data, $startPos, (int)($bit / IntegerType::BIT_8));
        if ($format !== null) { // if format is not null, then unpack
            $i = unpack($format, $payload)[1];
            $bin = decbin($i);
            return $i;
        } else { // byte by byte handling
            // TODO: just use format 'C*' ????
            $result = 0;
            $payloadLength = strlen($payload);
            for ($i = 0; $i < $payloadLength; $i++) {
                $result <<= IntegerType::BIT_8;
                $result |= $this->unpackInt8($payload, $i, $byteOrder);
            }
            return $result;
        }
    }

    /**
     * @param $data
     * @param int $startPos
     * @param int $byteOrder
     * @return int
     */
    protected function unpackInt8($data, int $startPos = 0, int $byteOrder = IntegerType::MSB): int
    {
        return $this->unpackInt(IntegerType::BIT_8, $data, $startPos, $byteOrder);
    }

    /**
     * @param $data
     * @param int $startPos
     * @param int $byteOrder
     * @return int
     */
    protected function unpackInt8Signed($data, int $startPos = 0, int $byteOrder = IntegerType::MSB): int
    {
        return $this->unpackInt(IntegerType::BIT_8_SIGNED, $data, $startPos, $byteOrder);
    }

    /**
     * @param $data
     * @param int $startPos
     * @param int $byteOrder
     * @return int
     */
    protected function unpackInt16($data, int $startPos = 0, int $byteOrder = IntegerType::MSB): int
    {
        return $this->unpackInt(IntegerType::BIT_16, $data, $startPos, $byteOrder);
    }

    /**
     * @param $data
     * @param int $startPos
     * @param int $byteOrder
     * @return int
     */
    protected function unpackInt32($data, int $startPos = 0, int $byteOrder = IntegerType::MSB): int
    {
        return $this->unpackInt(IntegerType::BIT_32, $data, $startPos, $byteOrder);
    }

    /**
     * @param $data
     * @param int $startPos
     * @param int $byteOrder
     * @return int
     */
    protected function unpackInt64($data, int $startPos = 0, int $byteOrder = IntegerType::MSB): int
    {
        return $this->unpackInt(IntegerType::BIT_64, $data, $startPos, $byteOrder);
    }

    /**
     * @param int $bit
     * @param int $integer
     * @param int $byteOrder
     * @return string
     */
    private function packInt(int $bit, int $integer, int $byteOrder = IntegerType::MSB): string
    {
        $format = IntegerType::FORMATTER[$byteOrder][$bit];
        return pack($format, $integer);
    }

    /**
     * @param int $integer
     * @return string
     */
    protected function packInt8(int $integer): string
    {
        return $this->packInt(IntegerType::BIT_8, $integer);
    }

    /**
     * @param int $integer
     * @return string
     */
    protected function packInt16(int $integer): string
    {
        return $this->packInt(IntegerType::BIT_16, $integer);
    }

    /**
     * @param int $integer
     * @return string
     */
    protected function packInt32(int $integer): string
    {
        return $this->packInt(IntegerType::BIT_32, $integer);
    }

    /**
     * @param int $integer
     * @return string
     */
    protected function packInt64(int $integer): string
    {
        return $this->packInt(IntegerType::BIT_64, $integer);
    }

}