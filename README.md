# ReactPHP DHCP-Server
Welcome to our PHP library that combines the power of ReactPHP with a native DHCP server framework written entirely in PHP! This innovative solution offers developers the ability to implement a DHCP (Dynamic Host Configuration Protocol) server within their PHP applications, leveraging the asynchronous and event-driven capabilities of ReactPHP.

By integrating ReactPHP into our DHCP server framework, we provide a high-performance and scalable solution that efficiently handles concurrent connections and maximizes resource utilization. This combination of ReactPHP and native PHP DHCP functionality offers developers a robust and flexible toolset for building network management applications, router configuration tools, and more.
## Installation
Install minimum PHP 8.1
```shell
# take care of install php8.1+ 
sudo apt install unzip php-cli php-xml php-curl php-zip php-mbstring -y
php -v
```
Install Composer
```shell
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php composer-setup.php
php -r "unlink('composer-setup.php');"
sudo mv composer.phar /usr/local/bin/composer
```
Clone Repository
```shell
git clone https://codeberg.org/volker.von.hoesslin/reactphp-dhcp-server.git
```

Define Test-Environment
```shell
# create test-interfaces
sudo ip link add ep1 type veth peer name ep2
sudo ip addr add "10.88.0.1/24" dev ep1
sudo ip link set ep1 up
sudo ip link set ep2 up
ip a
```

## Run example
```shell
cd reactphp-dhcp-server/example
composer install
sudo php server.php --interface="ep1"
```

## Self-Test
```shell
dhclient ep2
```
