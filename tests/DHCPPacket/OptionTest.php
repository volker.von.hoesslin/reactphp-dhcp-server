<?php

declare(strict_types=1);

namespace Tests\DHCPPacket;

use PHPUnit\Framework\TestCase;
use SkyDiablo\DhcpServer\DHCPPacket\Options\RawOption as Option;
use SkyDiablo\DhcpServer\DHCPPacket\Options\OptionInterface;
use SkyDiablo\DhcpServer\Exception\InvalidArgumentException;

class OptionTest extends TestCase
{
    public function testCreateOption(): void
    {
        $option = new Option(1, 4, "\x0A\x00\x00\x01");
        
        $this->assertInstanceOf(OptionInterface::class, $option);
        $this->assertEquals(1, $option->getCode());
        $this->assertEquals(4, $option->getLength());
        $this->assertEquals("\x0A\x00\x00\x01", $option->getValue());
    }

    public function testInvalidOptionLength(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Value length (3) does not match specified length (4)');
        new Option(1, 4, "\x0A\x00\x00"); // Zu kurzer Wert
    }

    public function testInvalidOptionLengthTooLong(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Value length (5) does not match specified length (4)');
        new Option(1, 4, "\x0A\x00\x00\x01\x02"); // Zu langer Wert
    }

    public function testZeroLength(): void
    {
        $option = new Option(1, 0, "");
        $this->assertEquals(0, $option->getLength());
        $this->assertEquals("", $option->getValue());
    }

    public function testMaximumLength(): void
    {
        $value = str_repeat("\x00", 255);
        $option = new Option(1, 255, $value);
        $this->assertEquals(255, $option->getLength());
        $this->assertEquals($value, $option->getValue());
    }

    public function testOptionEquality(): void
    {
        $option1 = new Option(1, 4, "\x0A\x00\x00\x01");
        $option2 = new Option(1, 4, "\x0A\x00\x00\x01");
        $option3 = new Option(1, 4, "\x0A\x00\x00\x02");
        
        $this->assertEquals($option1, $option2);
        $this->assertNotEquals($option1, $option3);
    }

    /**
     * @dataProvider invalidUTF8Provider
     */
    public function testInvalidUTF8Handling(string $value): void
    {
        $option = new Option(1, strlen($value), $value);
        $this->assertEquals($value, $option->getValue());
    }

    public function invalidUTF8Provider(): array
    {
        return [
            'Invalid UTF-8 sequence' => ["\xFF\xFE\xFD"],
            'Truncated UTF-8 sequence' => ["\xE2\x82"], // Unvollständiges Euro-Symbol
            'Invalid continuation byte' => ["\xE2\x28\xA1"],
        ];
    }

    public function testClone(): void
    {
        $original = new Option(1, 4, "\x0A\x00\x00\x01");
        $cloned = clone $original;
        
        $this->assertEquals($original, $cloned);
        $this->assertNotSame($original, $cloned);
    }
}