<?php

declare(strict_types=1);

namespace Tests\Manager;

use Darsyn\IP\Version\IPv4;
use PHPUnit\Framework\TestCase;
use SkyDiablo\DhcpServer\DHCPPacket\Options\IPv4Option;
use SkyDiablo\DhcpServer\DHCPPacket\Options\RawOption as Option;
use SkyDiablo\DhcpServer\DHCPPacket\Options\Serializer\IPv4Serializer as IPv4OptionSerializer;
use SkyDiablo\DhcpServer\DHCPPacket\Options\Serializer\Manager;
use SkyDiablo\DhcpServer\DHCPPacket\Options\Serializer\StringSerializer as StringOptionSerializer;
use SkyDiablo\DhcpServer\Exception\InvalidArgumentException;

class OptionManagerTest extends TestCase
{
    private Manager $manager;

    protected function setUp(): void
    {
        $this->manager = new Manager();
    }

    public function testRegisterSerializer(): void
    {
        $this->manager->registerSerializer(1, new IPv4OptionSerializer());
        $this->manager->registerSerializer(2, new StringOptionSerializer());
        
        $option = new Option(1, 4, "\xC0\xA8\x01\x01");
        $this->assertEquals('192.168.1.1', $this->manager->deserialize($option)->getValue());
        
        $option = new Option(2, 4, "test");
        $this->assertEquals('test', $this->manager->deserialize($option)->getValue());
    }

    public function testSerializeOption(): void
    {
        $this->manager->registerSerializer(1, new IPv4OptionSerializer());
        
        $option = new IPv4Option(1, IPv4::factory('192.168.1.1'));
        $binary = $this->manager->serialize($option);
        $this->assertEquals("\x01\x04\xc0\xa8\x01\x01", $binary);
    }

    public function testDeserializeOption(): void
    {
        $this->manager->registerSerializer(1, new IPv4OptionSerializer());
        
        $option = new Option(1, 4, "\xC0\xA8\x01\x01");
        $value = $this->manager->deserialize($option)->getValue();
        
        $this->assertEquals('192.168.1.1', $value);
    }

    public function testUnregisteredSerializer(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $option = new Option(999, 4, 'test');
        $this->manager->serialize($option);
    }
} 