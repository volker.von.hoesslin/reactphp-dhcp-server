<?php

declare(strict_types=1);

namespace Tests\Serializer;

use Darsyn\IP\Version\IPv4;
use PHPUnit\Framework\TestCase;
use SkyDiablo\DhcpServer\DHCPPacket\Options\IPv4Option;
use SkyDiablo\DhcpServer\DHCPPacket\Options\RawOption;
use SkyDiablo\DhcpServer\DHCPPacket\Options\Serializer\IPv4Serializer;
use SkyDiablo\DhcpServer\Exception\InvalidArgumentException;

class IPv4OptionSerializerTest extends TestCase
{
    private IPv4Serializer $serializer;

    protected function setUp(): void
    {
        $this->serializer = new IPv4Serializer();
    }

    public function testSerialize(): void
    {
        $option = new IPv4Option(1, IPv4::factory('192.168.1.1'));
        $result = $this->serializer->serialize($option);
        
        $this->assertEquals("\x01\x04\xc0\xa8\x01\x01", $result);
    }

    public function testDeserialize(): void
    {
        $binary = pack('N', ip2long('192.168.1.1'));
        $option = new RawOption(1, strlen($binary), $binary);
        
        $result = $this->serializer->deserialize($option);
        $this->assertEquals('192.168.1.1', (string)($result->getValue()));
    }

    public function testInvalidIPAddress(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $option = new \SkyDiablo\DhcpServer\DHCPPacket\Options\RawOption(1, 4, '256.256.256.256');
        $this->serializer->serialize($option);
    }
} 